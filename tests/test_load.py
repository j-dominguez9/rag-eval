import pytest
from src.rageval.generate.load import set_client, retrieve_sample
from qdrant_client import QdrantClient

from datasets import Dataset
from dotenv import load_dotenv

# import datasets

load_dotenv()


def test_set_client():
    client = set_client()
    assert isinstance(client, QdrantClient)


def test_retrieve_sample():
    client = set_client()
    sample = retrieve_sample(client=client, collection="RAG_chunks")
    print(len(sample))
    assert isinstance(sample, Dataset)
