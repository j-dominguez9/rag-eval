import pytest
from rageval.generate.questions import get_qa
from rageval.generate.load import set_client, retrieve_sample
from datasets import Dataset


qdrant_client = set_client()
dataset = retrieve_sample(client=qdrant_client, collection="RAG_chunks")


def test_get_qa():
    result = get_qa(dataset, num_questions=3)
    print(result)
    print(len(result))
    assert isinstance(result, Dataset)


result = get_qa(dataset, num_questions=3)
import pdb

pdb.set_trace()
