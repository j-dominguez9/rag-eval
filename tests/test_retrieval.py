from rageval.eval.retrieval import retriever_test
import pytest
from rageval.generate.questions import get_qa
from rageval.generate.load import set_client, retrieve_sample
import cohere
import os
from dotenv import load_dotenv

load_dotenv()


cohere_client = cohere.Client(os.getenv("COHERE_API_KEY"))

qdrant_client = set_client()
dataset = retrieve_sample(client=qdrant_client, collection="RAG_chunks")
result = get_qa(dataset, num_questions=3)


def test_retriever_test():
    acc = retriever_test(
        client=qdrant_client,
        collection="RAG_chunks",
        dataset=result,
        embed_model=cohere_client,
    )
    assert isinstance(acc, dict)
